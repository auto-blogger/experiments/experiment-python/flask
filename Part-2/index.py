from flask import Flask, render_template

app = Flask(__name__)

@app.route('/')
def index():
    variable = 'Hi, i am a variable'
    return render_template('index.html', variable=variable)

if __name__ == '__main__':
    app.run(debug=True)
